import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SearchForArrivalTime {

	public static void Search() throws FileNotFoundException {
		boolean hasQuit = false;
		while (hasQuit != true) {
			BST<String, String> tree = new BST<String, String>();
			fillBST(tree, "stop_times.txt");
			Scanner input = new Scanner(System.in);
			System.out
					.print("What Arrival Time are you looking for; Please enter in the format HH:MM:SS e.g. 17:53:22");
			String timeToSearch = input.next();
			timeToSearch = timeToSearch.replace(":", "");
			int timeToSearchInt = Integer.parseInt(timeToSearch);
			if (timeToSearch == null || timeToSearchInt > 235959 || timeToSearchInt < 0) {
				System.out.print("This is not a valid time");
				hasQuit = true;
			} else {
				System.out.print(tree.get(timeToSearch));
			}
		}
	}

	public static final void fillBST(BST<String, String> tree, String file) throws FileNotFoundException {
		Scanner input = new Scanner(new FileInputStream(file));
		boolean hasQuit = false;
		while (hasQuit != true) {
			while (input.hasNextLine()) {
				String[] lineInfo = input.nextLine().trim().split(",");
				String arrivalTimeString = lineInfo[1];
				arrivalTimeString = arrivalTimeString.replace(":", "");
				int arrivalTime = Integer.parseInt(arrivalTimeString);

				if (arrivalTimeString == null || arrivalTime > 235959 || arrivalTime < 0) {
					System.out.print("This is not a valid time");
					hasQuit = true;
				} else {
					String stopInformation = "// Trip id: " + lineInfo[0] + "// departure time: " + lineInfo[2]
							+ "// Stop ID : " + lineInfo[3] + "// Stop sequence: " + lineInfo[4] + "// Stop headsign: "
							+ lineInfo[5] + "// pickup type: " + lineInfo[6] + "// drop off type: " + lineInfo[7]
							+ "// shape distance travelled: " + lineInfo[8];
					tree.put(arrivalTimeString, stopInformation);
				}
			}
			input.close();
		}
	}
}
