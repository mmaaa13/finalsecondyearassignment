import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ShortestPath {

	public static double COST_FOR_TOP_TIMES = 1;
	public static double COST_FOR_TRANSFERS_TYPE_ZERO = 2;

	public static void shortestPathFinder() throws FileNotFoundException {
			EdgeWeightedDigraph shortestPath = new EdgeWeightedDigraph(0);
			fillGraphTransfers("transfers.txt", shortestPath);
			fillGraphStopTimes("stop_times.txt", shortestPath);
			System.out.print("What is the origin bus stop ID");
			Scanner input = new Scanner(System.in);
			int originStop = input.nextInt();
			System.out.print("What is the destination bus stop ID");
			int destinationStop = input.nextInt();
			DijkstraSP shorestPathSearch = new DijkstraSP(shortestPath, originStop);
			shorestPathSearch.hasPathTo(destinationStop);
			System.out.print("The cost of the shortest path to your destination is "
					+ shorestPathSearch.distTo(destinationStop));
			ArrayList<DirectedEdge> route = shortestPathRoute(shorestPathSearch, originStop, destinationStop);

			for (int i = (route.size() - 1); i >= 0; i--) {
				DirectedEdge edge = route.get(i);
				edge.toString();
			}
			input.close();

	}

	public static ArrayList<DirectedEdge> shortestPathRoute(DijkstraSP shortestPathSearch, int originEdge,
			int destinationEdge) {
		boolean finished = false;
		ArrayList<DirectedEdge> shortestPathRoute = new ArrayList<DirectedEdge>();
		DirectedEdge previousEdge = shortestPathSearch.edgeTo(destinationEdge);
		while (finished != true) {
			DirectedEdge currentEdge = shortestPathSearch.edgeTo(previousEdge.from());
			shortestPathRoute.add(currentEdge);

			if (currentEdge.from() == originEdge) {
				finished = true;
			}

		}
		return shortestPathRoute;
	}

	public static void fillGraphTransfers(String file, EdgeWeightedDigraph shortestPath) throws FileNotFoundException {

		Scanner input = new Scanner(new FileInputStream(file));

		while (input.hasNextLine()) {
			String[] lineInfo = input.nextLine().trim().split(",");

			String stopOneString = lineInfo[0];
			int stopOne = Integer.parseInt(stopOneString);

			String stopTwoString = lineInfo[1];
			int stopTwo = Integer.parseInt(stopTwoString);

			String transferTypeString = lineInfo[2];
			int transferType = Integer.parseInt(transferTypeString);
			double transferCostDouble = 0;

			if (transferType == 0) {
				transferCostDouble = COST_FOR_TRANSFERS_TYPE_ZERO;
			} else {
				String transferTime = lineInfo[3];
				double transferTimeDouble = Double.parseDouble(transferTime);
				transferCostDouble = (transferTimeDouble / 100);
			}

			DirectedEdge thisLineCostEdge = new DirectedEdge(stopOne, stopTwo, transferCostDouble);
			shortestPath.addEdge(thisLineCostEdge);
		}
		input.close();
	}

	public static void fillGraphStopTimes(String file, EdgeWeightedDigraph shortestPath) throws FileNotFoundException {

		Scanner input = new Scanner(new FileInputStream(file));

		String[] currentLine = null;
		String[] previousLine = null;
		int currentTripID = 0;
		int previousTripID = 0;
		while (input.hasNextLine()) {

			currentLine = input.nextLine().trim().split(",");

			String currentTripIDString = currentLine[0];
			currentTripID = Integer.parseInt(currentTripIDString);

			String destinationStopString = currentLine[3];
			int destinationStop = Integer.parseInt(destinationStopString);

			int originStop;
			if (previousLine != null) {
				String originStopString = previousLine[3];
				originStop = Integer.parseInt(originStopString);
			} else {
				originStop = 0;
			}

			if (currentTripID == previousTripID) {
				DirectedEdge thisLineDirectedEdge = new DirectedEdge(originStop, destinationStop, COST_FOR_TOP_TIMES);
				shortestPath.addEdge(thisLineDirectedEdge);

			}
			previousLine = currentLine;
			previousTripID = currentTripID;
		}

		input.close();
	}

}