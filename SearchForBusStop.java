import java.io.*;
import java.util.Scanner;

public class SearchForBusStop {

	public static void request() throws FileNotFoundException {
		boolean hasQuit = false;
		while (hasQuit == false) {
			System.out.print(
					"Would you like to search for the bus stop by full name (enter 1) or by the first few characters (enter 2)");
			Scanner input = new Scanner(System.in);
			if (input.hasNextInt()) {
				int decision = input.nextInt();
				if ((decision == 1) || (decision == 2)) {
					System.out.print("Enter Text");
					String textToSearch = "";
					Scanner userInput = new Scanner(System.in);
					textToSearch = userInput.next();
					if (decision == 1) {
						if (textToSearch == "") {
							System.out.print("This Bus stop is not in the system");
						} else {
							textToSearch = moveKeyWordFlagStop(textToSearch);
							TST.requestOptionOne(textToSearch);
						}
					} else {
						if (textToSearch == "") {
							System.out.print("This Bus stop is not in the system");
						} else {
							textToSearch = moveKeyWordFlagStop(textToSearch);
							TST.requestOptionTwo(textToSearch);
						}
					}
				} else {
					System.out.print("This is not a valid option, please enter 1 or 2");
					hasQuit = true;
				}
			} else {
				System.out.print("Please enter the interger; 1 or 2");
				hasQuit = true;
			}
		}
	}

	public static String moveKeyWordFlagStop(String stopName) {
		char[] stopNameCharacters = new char[stopName.length()];
		for (int i = 0; i < stopName.length(); i++) {
			stopNameCharacters[i] = stopName.charAt(i);
		}
		char thirdCharacter = stopNameCharacters[2];

		if (thirdCharacter == ' ') {
			String newStopName = "";

			char[] newStopNameCharacters = new char[stopName.length() + 3];

			for (int i = 0; i < stopName.length(); i++) {
				newStopNameCharacters[i] = stopName.charAt(i);
			}

			char firstLetter = newStopNameCharacters[0];
			char secondLetter = newStopNameCharacters[1];
			char emptySpace = newStopNameCharacters[2];

			newStopNameCharacters[newStopNameCharacters.length - 1] = secondLetter;
			newStopNameCharacters[newStopNameCharacters.length - 2] = firstLetter;
			newStopNameCharacters[newStopNameCharacters.length - 3] = emptySpace;

			String str = new String(newStopNameCharacters);

			newStopName = str.substring(3);
			return newStopName;
		} else {
			return stopName;
		}

	}
}