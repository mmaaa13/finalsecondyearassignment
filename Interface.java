import java.io.FileNotFoundException;
import java.util.Scanner;

public class Interface {
	public static void main(String[] args) throws FileNotFoundException {
		boolean hasQuit = false;
		while (hasQuit == false) {
			Scanner input = new Scanner(System.in);
			System.out.print("Would you like to search for a bus stop (Enter 1), Find the shortest route between stops (Enter 2), search bus trips by arrival times (Enter 3) or quit, (Enter any other integer) ");
			if (input.hasNextInt()) {
				int decision = input.nextInt();
				if (decision == 1) {
					SearchForBusStop.request();
				}
				else if (decision == 2) {
					ShortestPath.shortestPathFinder();
				}
				else if (decision == 3) {
					SearchForArrivalTime.Search();
				}
				else {
					System.out.print("You have Quit, thank you for using the programme!");
					hasQuit = true;
				}
			}
		}
	}
}
